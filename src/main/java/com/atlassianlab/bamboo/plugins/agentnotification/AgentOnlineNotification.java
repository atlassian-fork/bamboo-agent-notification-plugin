package com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.buildqueue.PipelineDefinition;
import com.atlassian.bamboo.event.agent.AgentRegisteredEvent;
import com.atlassian.bamboo.util.Narrow;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.EventObject;
import java.util.Map;

public class AgentOnlineNotification extends AgentChangeStatusNotification {
    private static final String TEXT_EMAIL_TEMPLATE = "notification-templates/AgentWentOnlineTextEmail.ftl";
    private static final String HTML_EMAIL_TEMPLATE = "notification-templates/AgentWentOnlineHtmlEmail.ftl";
    private static final String IM_TEMPLATE = "notification-templates/AgentWentOnlineTextIm.ftl";
    private static final String HTML_IM_TEMPLATE = "notification-templates/AgentWentOnlineHtmlIm.ftl";

    @Override
    protected String getTextImTemplate() {
        return IM_TEMPLATE;
    }

    @Override
    protected String getHtmlImTemplate() {
        return HTML_IM_TEMPLATE;
    }

    @Override
    protected String getHtmlEmailTemplate() {
        return HTML_EMAIL_TEMPLATE;
    }

    @Override
    protected String getTextEmailTemplate() {
        return TEXT_EMAIL_TEMPLATE;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Agent online notification";
    }

    @Override
    public String getEmailSubject() {
        final AgentRegisteredEvent event = Narrow.downTo(getPojoEvent(), AgentRegisteredEvent.class);
        if (event != null) {
            return getText("agent.notification.online.email.subject", event.getAgent().getName());
        }
        return StringUtils.EMPTY;
    }

    @Override
    protected <T extends EventObject> void populateAgentDetails(Map<String, Object> context, T event) {
        final AgentRegisteredEvent registeredEvent = Narrow.downTo(event, AgentRegisteredEvent.class);
        if (registeredEvent != null) {
            final PipelineDefinition agent = registeredEvent.getAgent();
            context.put(CFG_AGENT, new Agent(agent.getId(), agent.getName()));
        }
    }
}

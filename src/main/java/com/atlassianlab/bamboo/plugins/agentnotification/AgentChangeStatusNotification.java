package com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.notification.AbstractNotification;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.struts.TextProvider;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;

public abstract class AgentChangeStatusNotification extends AbstractNotification {
    private static final Logger log = Logger.getLogger(AgentChangeStatusNotification.class);
    static final String CFG_AGENT = "agent";

    @Inject
    @ComponentImport
    private TemplateRenderer templateRenderer;

    @Inject
    @ComponentImport
    private TextProvider textProvider;

    protected abstract String getTextImTemplate();

    protected abstract String getHtmlImTemplate();

    protected abstract String getHtmlEmailTemplate();

    protected abstract String getTextEmailTemplate();

    @Override
    public String getHtmlEmailContent() {
        return content(getHtmlEmailTemplate(), "Could not render email content",
                "Event is null, could not create Html Email content for " + getDescription());
    }

    @Override
    public String getIMContent() {
        return content(getTextImTemplate(), "Could not render IM content for " + getDescription(),
                "Event is null, could not create IM content for " + getDescription());
    }

    @Override
    public String getHtmlImContent() {
        return content(getHtmlImTemplate(), "Could not render IM content for " + getDescription(),
                "Event is null, could not create IM content for " + getDescription());
    }

    @Override
    public String getTextEmailContent() {
        return content(getTextEmailTemplate(), "Could not render email content",
                "Event is null, could not create Text Email content for " + getDescription());
    }

    @Nullable
    private String content(final String messageTemplate,
                           final String errorMessage,
                           final String eventIsNullErrorMessage) {
        final EventObject event = (EventObject) getPojoEvent();
        if (event == null) {
            log.error(eventIsNullErrorMessage);
            return null;
        }

        final Map<String, Object> context = new HashMap<>();

        populateContext(context, event);

        try {
            return templateRenderer.render(messageTemplate, context);
        } catch (Exception e) {
            log.error(errorMessage, e);
            return null;
        }
    }

    private void populateContext(Map<String, Object> context, EventObject event) throws IllegalStateException {
        context.put("notification", this);
        populateAgentDetails(context, event);
    }

    protected abstract <T extends EventObject> void populateAgentDetails(Map<String, Object> context, T event);

    protected String getText(String key, String... params) {
        return textProvider.getText(key, params);
    }
}
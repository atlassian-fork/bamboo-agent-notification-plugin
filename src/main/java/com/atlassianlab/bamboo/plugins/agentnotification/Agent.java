package com.atlassianlab.bamboo.plugins.agentnotification;

public class Agent {
    private final long id;
    private final String name;

    public Agent(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

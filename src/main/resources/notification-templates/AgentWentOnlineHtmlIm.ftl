[#-- @ftlvariable name="baseUrl" type="java.lang.String" --]
[#-- @ftlvariable name="i18n" type="com.atlassian.sal.api.message.I18nResolver" --]
[#-- @ftlvariable name="agent" type="com.atlassianlab.bamboo.plugins.agentnotification.Agent" --]
${i18n.getText("agent.notification.online.title", [agent.name?html])}. ${i18n.getText("agent.notification.view.agent.details.html", [baseUrl + "/agent/viewAgent.action?agentId=" + agent.id, agent.name?html])}
[#-- @ftlvariable name="baseUrl" type="java.lang.String" --]
[#-- @ftlvariable name="agent" type="com.atlassianlab.bamboo.plugins.agentnotification.Agent" --]
[#-- @ftlvariable name="i18n" type="com.atlassian.sal.api.message.I18nResolver" --]
${i18n.getText("agent.notification.offline.title", [agent.name?html])}. ${i18n.getText("agent.notification.view.agent.details.html", [baseUrl + "/agent/viewAgent.action?agentId=" + agent.id, agent.name?html])}
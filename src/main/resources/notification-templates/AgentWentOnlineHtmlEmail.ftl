[#-- @ftlvariable name="agent" type="com.atlassianlab.bamboo.plugins.agentnotification.Agent" --]
[#-- @ftlvariable name="agentName" type="java.lang.String" --]
[#include "templateHtml.ftl"][#lt/]
[@templateOuter baseUrl=baseUrl]
    <div id="title-status" class="successful">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
                <td id="title-status-icon">
                    <img src="${baseUrl}/images/iconsv4/icon-build-successful-w.png" alt="Agent online">
                </td>
                <td id="title-status-text">
                    ${i18n.getText("agent.notification.online.title", [agent.name?html])}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    [@showActions]
        <span class="email-list-item"><a href="${baseUrl}/agent/viewAgent.action?agentId=${agent.id}">View agent</a></span>
    [/@showActions]
[/@templateOuter]

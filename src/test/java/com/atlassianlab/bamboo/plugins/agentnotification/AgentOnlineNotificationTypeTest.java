package com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.agent.AgentType;
import com.atlassian.bamboo.buildqueue.PipelineDefinition;
import com.atlassian.bamboo.event.agent.AgentRegisteredEvent;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.context.ApplicationEvent;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class AgentOnlineNotificationTypeTest {
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
    @InjectMocks private AgentOnlineNotificationType onlineNotificationType;

    @Test
    @Parameters(method = "parametersForNotificationShouldBeConfigurable")
    public void notificationShouldBeConfigurable(final Object event,
                                                 final boolean expectedResult) {
        assertThat(onlineNotificationType.isNotificationRequired(event), is(expectedResult));
    }

    public Object[][] parametersForNotificationShouldBeConfigurable() {
        return new Object[][]{
                {createEvent(AgentType.ELASTIC), false},
                {createEvent(AgentType.REMOTE), true},
                {mock(ApplicationEvent.class), false}
        };
    }

    private AgentRegisteredEvent createEvent(AgentType agentType) {
        final PipelineDefinition agent = mock(PipelineDefinition.class);
        when(agent.getType()).thenReturn(agentType);
        return new AgentRegisteredEvent(this, agent);
    }
}
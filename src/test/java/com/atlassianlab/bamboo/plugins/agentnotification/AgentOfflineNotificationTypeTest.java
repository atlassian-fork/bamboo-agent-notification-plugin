package com.atlassianlab.bamboo.plugins.agentnotification;


import com.atlassian.bamboo.agent.AgentType;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.events.AgentOfflineEvent;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.context.ApplicationEvent;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class AgentOfflineNotificationTypeTest {
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
    @InjectMocks private AgentOfflineNotificationType offlineNotificationType;

    @Test
    @Parameters(method = "parametersForNotificationShouldBeConfigurable")
    public void notificationShouldBeConfigurable(final Object event,
                                                 final boolean expectedResult) {
        assertThat(offlineNotificationType.isNotificationRequired(event), is(expectedResult));
    }

    public Object[][] parametersForNotificationShouldBeConfigurable() {
        return new Object[][]{
                {createEvent(AgentType.ELASTIC), false},
                {createEvent(AgentType.REMOTE), true},
                {mock(ApplicationEvent.class), false}
        };
    }

    private AgentOfflineEvent createEvent(AgentType agentType) {
        final BuildAgent agent = mock(BuildAgent.class);
        when(agent.getType()).thenReturn(agentType);
        return new AgentOfflineEvent(this, agent);
    }
}